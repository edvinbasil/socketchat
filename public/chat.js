// Make socket connection
var url = window.location.origin
var socket = io.connect(url)

// Dom selection

var message = document.getElementById('message')
var handle = document.getElementById('handle')
var send = document.getElementById('send')
var output = document.getElementById('output')
var typing = document.getElementById('typing')
var chat_window=document.getElementById('chat_window')


//emit events

send.addEventListener('click',()=>{
    socket.emit('chat',{
        message: message.value,
        handle: handle.value
    })
})

message.addEventListener('keypress',(e)=>{
    if(e.key == 'Enter'){
        send.click()
        message.value=''
    }
    socket.emit("typing",handle.value)
})

// Listening for events

socket.on('chat',(data)=>{
    output.innerHTML+='<p><strong>' + data.handle + ': </strong>' + data.message + "</p>"
    typing.innerHTML=""
    chat_window.scrollTop=chat_window.scrollHeight
})


socket.on('typing',(data)=>{
    typing.innerHTML='<p><em>' + data + ' is typing a message...' +
        ' </em></p>'
    chat_window.scrollTop=chat_window.scrollHeight
})